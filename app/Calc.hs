{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeSynonymInstances #-}

import ExprT
import Parser
import StackVM
import qualified Data.Map as M
import System.Environment

eval :: ExprT -> Integer
eval (ExprT.Lit x) = x
eval (ExprT.Add x y) = eval x + eval y
eval (ExprT.Mul x y) = eval x * eval y

evalM :: Maybe ExprT -> Maybe Integer
evalM Nothing = Nothing
evalM (Just a) = Just (eval a)

evalStr :: String -> Maybe Integer
evalStr = evalM . parseExp ExprT.Lit ExprT.Add ExprT.Mul

class Expr a  where
  lit :: Integer -> a
  add :: a -> a -> a
  mul :: a -> a -> a

reify :: ExprT -> ExprT
reify = id

instance Expr ExprT where
  lit = ExprT.Lit
  add = ExprT.Add
  mul = ExprT.Mul

instance Expr Integer where
  lit = id
  add = (+)
  mul = (*)

instance Expr Bool where
  lit = (>0)
  add = (||)
  mul = (&&)

newtype MinMax = MinMax Integer deriving (Eq, Show)
instance Expr MinMax where
  lit = MinMax
  add (MinMax x) (MinMax y) =  MinMax (max x  y)
  mul (MinMax x) (MinMax y) =  MinMax (min x  y)

newtype Mod7 = Mod7 Integer deriving (Eq, Show)
instance Expr Mod7 where
  lit = Mod7
  add (Mod7 x) (Mod7 y) = Mod7 (mod (x+y) 7)
  mul (Mod7 x) (Mod7 y) = Mod7 (mod (x*y) 7)

instance Expr Program where
  lit x = [PushI x]
  add x y = x ++ y ++ [StackVM.Add]
  mul x y = x ++ y ++ [StackVM.Mul]

-- parseExpM :: Maybe ExprT -> Maybe Integer
-- parseExpM Nothing = Nothing
-- parseExpM (Just a) = Just (eval a)

compile :: String -> Maybe Program
compile str = parseExp lit add mul str :: Maybe Program

testExp :: Expr a => Maybe a
testExp = parseExp lit add mul "(3 * -4) + 5"
testInteger = testExp :: Maybe Integer
testBool = testExp :: Maybe Bool
testMM = testExp :: Maybe MinMax
testSat = testExp :: Maybe Mod7

class HasVars a where
  var :: String -> a
--
-- data VarExprT = Lit Integer
--                 | Var String
--                 | Add VarExprT VarExprT
--                 | Mul VarExprT VarExprT
--   deriving (Eq, Show)
--
-- instance HasVars VarExprT where
--   var = Var
--
-- instance Expr VarExprT where
--   add = Main.Add
--   mul = Main.Mul
--   lit = Main.Lit

instance HasVars (M.Map String Integer -> Maybe Integer) where
        var = M.lookup

instance Expr (M.Map String Integer -> Maybe Integer) where
        lit int0 _ = Just int0
        add var0 var1 map0 = do int0 <- var0 map0
                                int1 <- var1 map0
                                return (int0 + int1)
        mul var0 var1 map0 = do int0 <- var0 map0
                                int1 <- var1 map0
                                return (int0 * int1)

withVars :: [(String, Integer)]
            -> (M.Map String Integer -> Maybe Integer)
            -> Maybe Integer
withVars vs ex = ex $ M.fromList vs

main :: IO ()
main = do
  a <- getArgs
  print $ evalStr $ head a
  print "Done"
