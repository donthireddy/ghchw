module Main where
import System.Environment

main :: IO ()
main    = do
  print $ take 10 $ [1..]
  print "Done"
  return ()
