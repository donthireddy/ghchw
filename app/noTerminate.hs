module Main where

interleaveLists :: [a] -> [a] -> [a]
interleaveLists (x:xs) (y:ys) = x:interleaveLists xs ys

-- XXX The ruler function doesn't work in a lazy fashion.
--    if we use interleaveLists. Must use interleaveLists' below
-- See http://stackoverflow.com/questions/37174707/haskell-cis194-spring-13-homework-6-exercise-5
interleaveLists' :: [a] -> [a] -> [a]
interleaveLists' (x:xs) ys = x:interleaveLists' ys xs


interleaveInts :: Integer -> [Integer]
interleaveInts n
  = interleaveLists (repeat n) (interleaveInts (n+1))

ruler :: [Integer]
ruler = interleaveInts 0

-- which corresponds to the ruler function
-- 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 4, . . .
-- where the nth element in the stream (assuming the first element
-- corresponds to n = 1) is the largest power of 2 which evenly
-- divides n.
main :: IO ()
main    = do
  print "Starting. Alternating 1's and 2's. Should be easy"
  print $ take 10 $ interleaveLists (repeat 1) (repeat 2)
  print "A more"
  print " A miracle if this terminates and returns a value"
  print $ take 50 $ interleaveInts 0
  print "Done"
  return ()
