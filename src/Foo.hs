module Foo where
import qualified Data.Map as M
foo = do
    let emptyMap = M.empty
        mapWithKeys = M.insert 5 "Four" emptyMap
        mapWithKeys' = M.insert 5 "Five" mapWithKeys
    putStrLn $ mapWithKeys' M.! 5

interleaveLists :: [a] -> [a] -> [a]
interleaveLists (x:xs) (y:ys) = x:(y:interleaveLists xs ys)

inh :: Integer -> [Integer]
inh n
-- XXX This function doesn't work in a lazy way if
--     don't limit n. Don't understand why
  = interleaveLists (repeat n)
    (inh (n+1))
