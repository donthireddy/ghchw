-- http://blog.sigfpe.com/2007/04/trivial-monad.html
module TrivialMonad where

data W a = W    a  deriving Show

instance Applicative W where
  pure = W
  W f <*> W x = W (f x)

instance Functor W where
  fmap f (W x) = W (f x)

instance Monad W where
  return = W
  W x >>= f  = f x

f :: Int -> W Int
f x = W (x + 1)

g :: Int -> W Int -> W Int
-- g x (W y) = W (x+y)
g x y =  y >>= return . (+x)

h :: W Int -> W Int -> W Int
h x y =  x >>= flip g y

join :: W (W a) -> W a
join x = x >>= id
