module CreditCard where

toDigits :: Integer -> [Integer]
toDigits 0 = []
toDigits n
    | n < 0  = []
    | n < 10 = [n]
    | otherwise = toDigits (n `div` 10) ++ [n `mod` 10]

toDigitsRev :: Integer -> [Integer]
toDigitsRev = reverse . toDigits

doubleEveryOther :: [Integer] -> [Integer]
doubleEveryOther = reverse . doubleEveryOtherLeft . reverse

doubleEveryOtherLeft :: [Integer] -> [Integer]
doubleEveryOtherLeft [] = []
doubleEveryOtherLeft [x] = [x]
doubleEveryOtherLeft (x:y:xs) = [x, 2*y] ++ doubleEveryOtherLeft xs

sumDigits :: [Integer] -> Integer
sumDigits = sum . concatMap toDigits

isTen :: Integer -> Bool
isTen n = 0 == n `mod` 10

validate :: Integer -> Bool
validate = isTen . sumDigits . doubleEveryOther . toDigits
