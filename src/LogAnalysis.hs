{-# OPTIONS_GHC -Wall #-}
module LogAnalysis where
import Log

makeRecord :: [String] -> LogMessage
makeRecord ("I":ts:rest) = LogMessage Info (read ts::Int) (unwords rest)
makeRecord ("W":ts:rest) = LogMessage Warning (read ts::Int) (unwords rest)
makeRecord ("E":sev:ts:rest) = LogMessage (Error (read sev::Int)) (read ts::Int) (unwords rest)
makeRecord ws = Unknown $ unwords ws

parseMessage :: String -> LogMessage
parseMessage =  makeRecord . words

parse :: String -> [LogMessage]
parse = map parseMessage . lines

getTs :: LogMessage -> Int
getTs (Unknown _) = 0
getTs (LogMessage _ ts _) = ts

getSev :: LogMessage -> Int
getSev (LogMessage (Error sev) _ _) = sev
getSev _ = 0

getMsg :: LogMessage -> String
getMsg (Unknown msg) = msg
getMsg (LogMessage _ _ msg) = msg

isSerious :: LogMessage -> Bool
isSerious = (<=) 50 . getSev

insert :: LogMessage -> MessageTree -> MessageTree
insert msg Leaf = Node Leaf msg Leaf
insert msgNew (Node l msgCurr r)
  | getTs msgNew < getTs msgCurr = Node (Node Leaf msgNew Leaf) msgCurr r
  | otherwise        = Node l msgCurr  (Node Leaf msgNew Leaf)


inOrder :: MessageTree -> [LogMessage]
inOrder Leaf = []
inOrder (Node l msg r) = inOrder l ++ [msg] ++ inOrder r

whatWentWrong :: [LogMessage] -> [String]
whatWentWrong = map getMsg . filter isSerious

buildMsgTree :: [LogMessage] -> MessageTree
buildMsgTree = foldr insert Leaf

orderedMessages :: [LogMessage] -> [LogMessage]
orderedMessages = inOrder . buildMsgTree
