{-# LANGUAGE FlexibleInstances, TypeSynonymInstances #-}
module JoinList where
import Data.Monoid
import Sized
import Scrabble
import Editor
import Buffer

data JoinList m a = Empty
  | Single m a
  | Append m (JoinList m a) (JoinList m a)
  deriving (Eq, Show)

(+++) :: Monoid m => JoinList m a -> JoinList m a -> JoinList m a
x +++ y = Append (mappend (tag x) (tag y)) x y

instance Buffer (JoinList (Score, Size) String) where
  toString Empty = ""
  toString (Single m str) = str
  toString (Append m l r) = toString l ++ toString r

  fromString str = buildBalanced $ lines str
  line = indexJ
  replaceLine n s b = takeJ n b +++ buildBalanced [s] +++ dropJ (n+1) b
  numLines Empty = 0
  numLines (Single _ _) = 1
  numLines (Append (_, Size n) _ _) =  n
  value (Append (Score v, _) _ _) =  v

--  You may find it helpful to implement a helper function
tag :: Monoid m => JoinList m a -> m
tag Empty = mempty
tag (Single m _) = m
tag (Append m _ _) = m

indexJ :: (Sized b, Monoid b) => Int -> JoinList b a -> Maybe a
indexJ _ Empty = Nothing
indexJ 0 (Single m a) = Just a
indexJ _ (Single m a) = Nothing
indexJ i (Append m l r)
  | i < 0 || i >= getSize (size m)  = Nothing
  | i < getSize (size $ tag l) = indexJ i l
  | otherwise        = indexJ (i - getSize  (size $ tag l)) r

dropJ :: (Sized b, Monoid b) => Int -> JoinList b a -> JoinList b a
dropJ _ Empty = Empty
dropJ 0 jl = jl
dropJ _ (Single m a) = Empty
dropJ i jl@(Append m l r)
  | i < 0   = jl
  | i >= getSize (size m)  = Empty
  | i < getSize (size $ tag l) = dropJ i l +++ r
  | otherwise        = dropJ (i - getSize  (size $ tag l)) r

takeJ :: (Sized b, Monoid b) => Int -> JoinList b a -> JoinList b a
takeJ _ Empty = Empty
takeJ 0 jl = Empty
takeJ _ jl@(Single m a) = jl
takeJ i jl@(Append m l r)
  | i < 0   = Empty
  | i >= getSize (size m)  = jl
  | i < getSize (size $ tag l) = takeJ i l
  | otherwise        = l +++ takeJ (i - getSize (size $ tag l)) r

scoreLine :: String -> JoinList Score String
scoreLine str = Single (scoreString str) str

buildBalanced []   = Empty
buildBalanced [s] = Single (scoreString s, Size 1) s
buildBalanced elts = buildBalanced (take half elts) +++
                     buildBalanced (drop half elts)
    where half = length elts `quot` 2

main = runEditor editor $ buildBalanced
         [ "This buffer is for notes you don't want to save, and for"
         , "evaluation of steam valve coefficients."
         , "To load a different file, type the character L followed"
         , "by the name of the file."
         ]
