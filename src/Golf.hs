-- Week 3 HW
module Golf where
import Data.List
--
-- Take every nth element from list
--
every :: Int -> [a] -> [a]
every n xs = take (div (length xs) n) [xs !! i | i <- [n-1, 2*n-1..]]

-- Return list of :
-- a list with every element,
-- a list with every 2nd element,
-- ...
-- a list with every nth element
-- of the input list, where n is the lenth of the list
skips :: [a] -> [[a]]
skips xs = map (($ xs) . every) [1..length xs]


-- Local maxima
--
isPeak :: Ord a => [a] -> Bool
isPeak [x,y,z] = y > x && y > z
isPeak _ = False

windows :: Int -> [a] ->[[a]]
windows n xs = map (take n) (tails xs)

localMaxima :: Ord a => [a] -> [a]
localMaxima xs =  map (!!1) $ filter isPeak ws
  where ws = windows 3 xs

-- Histogram
--
countOcc :: [Integer ] -> Int -> Int
countOcc xs n = length $ filter (==fromIntegral n) xs

getCounts :: [Integer] -> [Int]
getCounts xs = flip map [0..9] $ countOcc xs

getStars :: [Int] -> [String]
getStars xs = map (\n -> replicate n '*' ++ replicate (maxX-n) ' ' ) xs
  where maxX = maximum xs

histogram :: [Integer] -> String
histogram xs = intercalate "\n" (reverse  $ transpose $ getStars $ getCounts xs)
  ++ "\n==========\n0123456789\n"
