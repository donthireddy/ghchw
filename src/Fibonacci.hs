{-# LANGUAGE FlexibleInstances #-}
{-# OPTIONS_GHC -fno-warn-missing-methods #-}
module Fibonacci where
fib :: Integer -> Integer
fib 0 = 0
fib 1 = 1
fib n = fib (n-1) + fib (n-2)
fibs1 = map fib [0..]

data Stream a = Cons a (Stream a)

tail :: Stream a -> Stream a
tail (Cons x str) = str

streamToList :: Stream a -> [a]
streamToList (Cons x str) = x:streamToList str

instance Show a => Show (Stream a) where
  show str = showList $ take 20 $ streamToList str where
    showList [] = "()"
    showList (x:xs) = "(Cons " ++ show x ++ " " ++ showList xs ++ ")"

streamRepeat :: a -> Stream a
streamRepeat x = Cons x (streamRepeat x)

streamMap :: (a -> b) -> Stream a -> Stream b
streamMap f (Cons x str) = Cons (f x) $ streamMap f str

streamFromSeed :: (a -> a) -> a -> Stream a
streamFromSeed f x = Cons x $ streamFromSeed f $ f x

nats :: Stream Integer
nats = streamFromSeed (+1) 0

-- See http://stackoverflow.com/questions/37174707/haskell-cis194-spring-13-homework-6-exercise-5
-- for why interleaveStreams is written this way
interleaveStreams :: Stream a -> Stream a -> Stream a
interleaveStreams (Cons s1 s1rest) s2s = Cons s1
  (interleaveStreams s2s s1rest)

interleaveNsAndHigher :: Integer -> Stream Integer
interleaveNsAndHigher n = interleaveStreams (streamRepeat n)
    (interleaveNsAndHigher (n+1))


ruler :: Stream Integer
ruler = interleaveNsAndHigher 0

instance Num (Stream Integer) where
  fromInteger n = Cons n (streamRepeat 0)
  negate (Cons n str) = Cons (-1 * n) (negate str)
  (Cons x restx) + (Cons y resty) = Cons (x+y) $ restx + resty
  (Cons a0 a') * (Cons b0 b') =
    Cons (a0*b0) (streamMap (*a0) b' + a' * Cons b0 b')

instance Fractional (Stream Integer) where
        (Cons a0 a') / (Cons b0 b') = q where
            tr x0 = floor (fromIntegral x0 / fromIntegral b0::Double)
            hed = floor (fromIntegral a0 / fromIntegral b0::Double)
        --- Q = (a0/b0) + x (A' - QB') magic lol
            q = Cons hed (streamMap tr (a' - (q * b')))

fibs3 :: Stream Integer
fibs3 = x / ( 1 - x - x^2)

x :: Stream Integer
x = Cons 0 (Cons 1 (streamRepeat 0))

data Matrix = Matrix Integer Integer
                     Integer Integer
instance Num Matrix where
  (Matrix a11 a12 a21 a22) * (Matrix b11 b12 b21 b22) =
    Matrix (a11*b11+a12*b21) (a11*b12+a12*b22)
           (a21*b11+a22*b21) (a21*b12+a22*b22)

fib4 :: Integer -> Integer
fib4 0 = 0
fib4 n = let f = Matrix 1 1
                        1 0 in
         let fn (Matrix _ x _ _) = x
         in fn (f^n)
