module Party where
import Data.Tree
import Data.Tuple
import Employee

glCons :: Employee -> GuestList -> GuestList
glCons emp@(Emp _ efun) (GL emps fun) = GL (emp:emps) (fun + efun)

instance Monoid GuestList where
  mempty = GL [] 0
  mappend (GL emps1 fun1) (GL emps2 fun2) = GL (emps1 ++ emps2) (fun1 + fun2)

moreFun :: GuestList -> GuestList -> GuestList
moreFun = max -- gl1@(GL _ fun1) gl2@(GL _ fun2) = if fun1 >= fun2 then gl1 else gl2

forestFold :: (b -> a -> b) -> b -> Forest a -> b
forestFold _ b1 [] = b1
forestFold f1 b1 (t1:ts) = forestFold f1 (treeFold f1 b1 t1) ts

treeFold :: (b -> a -> b) -> b -> Tree a -> b
treeFold f b0 (Node label subForest) =  f (forestFold f b0 subForest) label
  where
    lbl (Node l _) = l

--glPairSum :: (GuestList, GuestList) -> (GuestList, GuestList) -> (GuestList, GuestList)
--glPairSum = mappend

nextLevel :: Employee -> Forest (GuestList, GuestList) -> (GuestList, GuestList)
nextLevel emp  = swap . forestFold mappend (mempty, GL [emp] (empFun emp))

maxFunPair :: Tree Employee -> (GuestList, GuestList)
maxFunPair (Node emp []) = (GL [emp] (empFun emp), GL [] 0
maxFunPair (Node emp emps) = treeFold

maxFun :: Tree Employee -> GuestList
maxFun (Node emp []) = GL [emp] (empFun emp)
maxFun (Node emp emps) = treeFold  (GL [] 0)
