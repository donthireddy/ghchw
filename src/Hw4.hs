module Hw4 where

shrinkEven :: Integer -> Integer
shrinkEven n = if even n then n-2 else n

fun1 :: [Integer] -> Integer
fun1 = product . map shrinkEven

fun2 :: Integer -> Integer
fun2 = undefined

data Tree a = Leaf
  | Node Integer (Tree a) a (Tree a)
  deriving (Show, Eq)

foldTree :: [a] -> Tree a
foldTree = foldr addElem Leaf

newNode :: Integer -> a -> Tree a
newNode n el = Node n Leaf el Leaf

getHt :: Tree a -> Integer
getHt Leaf = 0
getHt (Node n _ _ _) = n

addElem :: a -> Tree a -> Tree a
addElem x Leaf = Node 0 Leaf x Leaf
addElem el (Node n Leaf x Leaf) =
  Node (n+1) (newNode n el) x Leaf
addElem el (Node n Leaf x nd) =
  Node n (newNode (n-1) el) x nd
addElem el (Node n nd x Leaf) =
  Node n nd x (newNode (n-1) el)
addElem el (Node n nd1 x nd2) =
  if getHt nd1 <= getHt nd2
     then Node (n3+1) nd3 x nd2
     else Node (n3+1) nd1 x nd3
  where nd = if getHt nd1 <= getHt nd2 then nd1 else nd2
        nd3 = addElem el nd
        n3 = getHt nd3
