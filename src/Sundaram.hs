module Sundaram where
import Data.List
import qualified Data.Set as Set

-- does this pair
sundaramFilter :: Int -> (Int, Int) -> Bool
sundaramFilter n (i,j) = i + j + 2*i*j <= n

cartProd :: [a] -> [b] -> [(a,b)]
cartProd xs ys = [(x,y) | x <- xs, y <- ys]

deleteSet :: Int -> Set.Set Int
deleteSet n = Set.fromList $ filter (<=n) $ map (\(i,j) -> i + j + 2*i*j) $ cartProd [1..n] [1..n]

--deleteSet :: Int -> Set.Set Int
--deleteSet n = Set.fromList $ map (\(i,j)->i+j+2*i*j) $ filter (sundaramFilter n) $ cartProd [1..n] [1..n]

keepSet :: Int -> Set.Set Int
keepSet n = Set.fromList [1..n] `Set.difference` deleteSet n

primes :: Int -> Set.Set Int
primes n = Set.map (\n->2*n+1) $ keepSet n
